type roomPageError =
  | RoomPageInitializing
  | RoomPageErrorResponse
  | RoomPagePageExists
  | RoomPageNoRoom
  | AuthError;

type userName = string;
type character = string;
type userAssignment =
  | UserAssignment(userName, character);

type userAssignmentRecord = {
  name: userName,
  character,
};

/* todo: status should have it's own variant type */
type roomName = string;
type status = string;
type roomInfo =
  | RoomInfo(roomName, status, array(string), array(userAssignment));

type roomInfoRecord = {
  roomName,
  status,
  admin: array(string),
  otherPlayers: array(userAssignmentRecord),
};

/* todo: use a non-exception based JSON decoder library */

let userAssignmentRecord = json =>
  Json.Decode.{
    name: json |> field("name", string),
    character: json |> field("character", string),
  };

let userAssignmentFromRecord = u => UserAssignment(u.name, u.character);

let userAssignment =
  Json.Decode.(userAssignmentRecord |> map(userAssignmentFromRecord));

let roomInfoRecord = json =>
  Json.Decode.{
    roomName: json |> field("roomName", string),
    status: json |> field("status", string),
    admin: json |> field("admin", array(string)),
    otherPlayers: json |> field("otherPlayers", array(userAssignmentRecord)),
  };

let roomInfo =
  Json.Decode.(
    roomInfoRecord
    |> map(r => {
         RoomInfo(
           r.roomName,
           r.status,
           r.admin,
           Array.map(userAssignmentFromRecord, r.otherPlayers),
         )
       })
  );

type simplePass = string;
type userData =
  | UserData(userName, simplePass);
type userDataRecord = {
  userName,
  simplePass,
};

let toUserDataRecord: userData => userDataRecord =
  fun
  | UserData(userName, simplePass) => {userName, simplePass};

let userDataJson = ud =>
  Json.Encode.(
    object_([
      ("userName", ud.userName |> string),
      ("simplePass", ud.simplePass |> string),
    ])
  );
