open Models;

/* axios wants to use Js.t objects, despite they being insecure :S  */
external jsonToObjects: Js.Json.t => Js.t({..}) = "%identity";

let getStatus: Js.Promise.error => int = [%bs.raw
  {| (e) => e.response && e.response.status || 200 |}
];

let getRoomInfo = (server, userData, room, f) => {
  let user: Axios_types.auth =
    switch (userData) {
    | UserData(username, password) => {
        "username": username,
        "password": password,
      }
    };
  let c = Axios.makeConfig(~auth=user, ());
  Js.Promise.(
    Axios.getc({j|$server/game/$room|j}, c)
    |> then_(response => resolve(response##data))
    |> then_(json => f(Ok(json->roomInfo)) |> resolve)
    |> catch(e => {
         switch (getStatus(e)) {
         | 404 => f(Error(RoomPageNoRoom))
         | 401 => f(Error(AuthError))
         | _ => f(Error(RoomPageErrorResponse))
         };
         resolve();
       })
    |> ignore
  );
  None;
};

let createRoom = (server, room, userData, f) => {
  Js.Promise.(
    Axios.postData(
      {j|$server/game/$room|j},
      userData |> toUserDataRecord |> userDataJson |> jsonToObjects,
    )
    |> then_(response => resolve(response##data))
    |> then_(json => {
         AuthStorage.setAuth(room, userData);
         f(Ok(json->roomInfo)) |> resolve;
       })
    |> catch(e => {
         switch (getStatus(e)) {
         | 200 => f(Error(RoomPageErrorResponse))
         | _ => f(Error(RoomPagePageExists))
         };
         resolve();
       })
    |> ignore
  );
  None;
};

type setRoomInfo = result(roomInfo, roomPageError) => unit;

type roomService = {
  getRoomInfo: (userData, string, setRoomInfo) => option(unit => unit),
  createRoom: (string, userData, setRoomInfo) => option(unit => unit),
};

let make: string => roomService =
  server => {
    getRoomInfo: getRoomInfo(server),
    createRoom: createRoom(server),
  };
