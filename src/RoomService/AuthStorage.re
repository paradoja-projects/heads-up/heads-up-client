let storage = Dom.Storage2.sessionStorage;

let setAuth = (room, userData) =>
  switch (userData) {
  | Models.UserData(u, p) =>
    Dom.Storage2.setItem(storage, room, {j|$u\n$p|j})
  };

let getAuth = room => {
  let value = Dom.Storage2.getItem(storage, room);
  let toUserData =
    fun
    | [|u, p|] => Some(Models.UserData(u, p))
    | _ => None;
  Belt.Option.(value->map(Js.String.split("\n"))->flatMap(toUserData));
};
