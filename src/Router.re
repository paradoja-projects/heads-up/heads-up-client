[@react.component]
let make = (~server) => {
  let url = ReasonReactRouter.useUrl();
  let roomService: RoomService.roomService = RoomService.make(server);

  switch (url.path) {
  | ["game", roomName] => <Room roomName roomService />
  | _ => <RoomChooser />
  };
};
