[@react.component]
let make = (~otherPlayers) =>
  <ul>
    {otherPlayers
     |> Array.map(ui => {
          let Models.UserAssignment(name, character) = ui;
          <li key=name> {React.string({j|$name, $character|j})} </li>;
        })
     |> React.array}
  </ul>;
