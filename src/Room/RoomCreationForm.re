[@react.component]
let make = (~roomName, ~roomService: RoomService.roomService, ~setRoomInfo) => {
  let (userName, setUserName) = React.useState(() => "");
  let (simplePass, setSimplePass) = React.useState(() => "");

  let createRoom = event => {
    ReactEvent.Form.preventDefault(event);
    let userData: Models.userData = UserData(userName, simplePass);
    roomService.createRoom(roomName, userData, r => setRoomInfo(_ => r))
    |> ignore;
    ();
  };

  let handleInput = (f, event) => f(event->ReactEvent.Form.target##value);

  <div>
    <p>
      {React.string(
         "This game doesn't exist yet. But you can create a new one now if you want!",
       )}
      <br />
      {React.string("You'll be the organizer of a new game of HeadsUp.")}
    </p>
    <p> {React.string("Just give us your name and a fruit to start:")} </p>
    <form onSubmit=createRoom>
      <input
        name="userName"
        value=userName
        placeholder="name (George)"
        onChange={handleInput(setUserName)}
      />
      <br />
      <input
        name="simplePass"
        value=simplePass
        placeholder="fruit (orange)"
        onChange={handleInput(setSimplePass)}
      />
      <input type_="submit" value="Create room" />
    </form>
  </div>;
};
