let headsUp =
  Webapi.Dom.document
  |> Webapi.Dom.Document.getElementById("headsUp")
  |> Belt.Option.getExn;
let server =
  headsUp |> Webapi.Dom.Element.getAttribute("server") |> Belt.Option.getExn;

ReactDOMRe.render(<Router server />, headsUp);
