[@react.component]
let make = (~roomName, ~roomService: RoomService.roomService) => {
  let (roomInfo, setRoomInfo) =
    React.useState(() => Error(Models.RoomPageInitializing));

  React.useEffect0(() =>
    roomService.getRoomInfo(Models.UserData("", ""), roomName, r =>
      setRoomInfo(_ => r)
    )
  );

  <div>
    {React.string(roomName)}
    <br />
    <a
      href="/"
      onClick={e => {
        ReactEvent.Mouse.preventDefault(e);
        ReasonReactRouter.push("/");
      }}>
      {React.string("Home")}
    </a>
    {switch (roomInfo) {
     | Error(Models.RoomPageInitializing) =>
       <div> {React.string("Initializing")} </div>
     | Error(Models.AuthError) => <div> {React.string("Auth error.")} </div>
     | Error(Models.RoomPageNoRoom) =>
       <RoomCreationForm roomName roomService setRoomInfo />
     | Error(RoomPagePageExists) =>
       React.string("Error, someone seems to have just created this page!")
     | Error(_) => React.string("Error, please reload page")
     | Ok(Models.RoomInfo(_, _status, _admin, otherPlayers)) =>
       <RoomDuringPlay otherPlayers />
     }}
  </div>;
};
