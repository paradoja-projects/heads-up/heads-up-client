[@react.component]
let make = () => {
  let (room, setRoom) = React.useState(() => "");

  let enterRoom = event => {
    ReactEvent.Form.preventDefault(event);
    ReasonReactRouter.push({j|/game/$room|j});
  };

  let handleChangeRoom = event =>
    setRoom(event->ReactEvent.Form.target##value);

  <div>
    <form onSubmit=enterRoom>
      <input name="room" value=room onChange=handleChangeRoom />
      <input type_="submit" value="Enter room" />
    </form>
  </div>;
};
